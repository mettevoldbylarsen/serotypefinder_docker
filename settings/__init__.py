#!/usr/bin/env python
#

import os
from dev import DEPENDENCIES_DEV
from prod import DEPENDENCIES_PROD

__version__ = "0.0.1"

SETTINGS = {
    'PROD': DEPENDENCIES_PROD,
    'DEV': DEPENDENCIES_DEV,
}
if 'INSTALLING_MODE' in os.environ:
    print '... Using ', os.environ['INSTALLING_MODE'], ' mode'
    DEPENDENCIES = SETTINGS[os.environ['INSTALLING_MODE']]
else:
    print '... ENV variable INSTALLING_MODE not set. Default used, DEVELOPMENT'
    DEPENDENCIES = SETTINGS['DEV']
