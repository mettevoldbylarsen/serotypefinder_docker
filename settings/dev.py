#!/usr/bin/env python
#

DEPENDENCIES_DEV = [
    # KmerFinder
    'git+https://bitbucket.org/genomicepidemiology/kmer-finder#egg=KmerFinder',
]
