#!/usr/bin/env python
'''
################################################################################
##########                    CGE Service Wrapper                    ###########
################################################################################
'''
import sys, os, json

# INCLUDING THE CGE MODULES (No need to change this part)
from cge import check_file_type, debug, get_arguments, proglist, Program, open_, adv_dict 

# SET GLOBAL VARIABLES
service, version = "SerotypeFinder", "1.1" 

def main():
   ''' MAIN '''
   # PARSE ARGUMENTS
   # Add service specific arguments using the following format:
   #  (OPTION, VARIABLE,  DEFAULT,  HELP)
   args = get_arguments([
      ('-f', 'contigs', None, 'The input file (fasta file required)'),
      ('-k', 'threshold', '90.00', ('Identity threshold for reporting a BLAST hit.'
                                    )),
      ('-l', 'minlength', '0.60', ('Minimum gene coveraage threshold for reporting'
                                   ' a BLAST hit.')),
      ('-d', 'db_dir', '/databases/serotypefinder', 'Path to database directory'),
      ('-s', 'databases', None,
       # ('ecoli'),
       'Comma-separated list of databases to BLAST the input sequences against.'
       ),
      ('-w', 'workdir', None, 'The working directory')
      ])
   
   # VALIDATE AND HANDLE ARGUMENTS
   if args.workdir is not None:
      os.chdir(args.workdir)
   if args.db_dir is None:
      debug.graceful_exit("Input Error: No database directory was provided!\n")
   elif not os.path.exists(args.db_dir):
      debug.graceful_exit("Input Error: The specified database directory does not"
                          " exist!\n")
   else:
      # Check existence of config file
      db_config_file = '%s/config'%(args.db_dir)
      if not os.path.exists(db_config_file):
         debug.graceful_exit("Input Error: The database config file could not be "
                             "found!")
   
   if args.contigs is None:
      debug.graceful_exit("Input Error: No Contigs were provided!\n")
   elif not os.path.exists(args.contigs):
      debug.graceful_exit("Input Error: Contigs file does not exist!\n")
   elif check_file_type(args.contigs) != 'fasta':
      debug.graceful_exit(('Input Error: Invalid contigs format (%s)!\nOnly the '
                           'fasta format is recognised as a proper contig format.'
                           '\n')%(check_file_type(args.contigs)))
   else:
       args.contigs = os.path.abspath(args.contigs)
   
   if args.databases is '':
      debug.graceful_exit("Input Error: No database was specified!\n")
   else:
      dbs = adv_dict({})
      extensions = []
      with open_(db_config_file) as f:
         for l in f:
            l = l.strip()
            if l == '': continue
            if l[0] == '#':
               if 'extensions:' in l:
                  extensions = [s.strip() for s in l.split('extensions:')[-1].split(',')]
               continue
            tmp = l.split('\t')
            if len(tmp) != 4:
               debug.graceful_exit(("Input Error: Invalid line in the database"
                                    " config file!\nA proper entry requires 4 tab "
                                    "separated columns!\n%s")%(l))
            db_prefix1 = tmp[0].strip()
            db_prefix2 = tmp[1].strip()
            name = tmp[2].split('#')[0].strip()
            description = tmp[3]
            # Check if all db files are present
            for ext in extensions:
               db_path = "%s/%s.%s"%(args.db_dir, db_prefix1, ext)
               if not os.path.exists(db_path):
                  debug.graceful_exit(("Input Error: The database file (%s) "
                                       "could not be found!")%(db_path))
            if name not in dbs: dbs[name] = []
            print name
            dbs[name].append(db_prefix1)
            for ext in extensions:
               db_path = "%s/%s.%s"%(args.db_dir, db_prefix2, ext)
               if not os.path.exists(db_path):
                  debug.graceful_exit(("Input Error: The database file (%s) "
                                       "could not be found!")%(db_path))
            if name not in dbs: dbs[name] = []
            print name
            dbs[name].append(db_prefix2)
      if len(dbs) == 0:
         debug.graceful_exit("Input Error: No databases were found in the "
                             "database config file!")
      #if len(dbs) == 1:
      #   debug.graceful_exit("Input Error: Only one database was found in the "
      #                       "database config file! At least two are required")
      dbs = dbs.invert()
      
      if args.databases is None:
         # Choose all available databases from the config file
         databases = dbs.keys()
         print databases
      #else: #Udkommenteret - der er kun en database for serotypefinder (ecoli), saa ingen grund til at skrive noget  
         # Handle multiple databases
      #   args.databases = args.databases.split(',')
         # Check that the ResFinder DBs are valid
      #   databases = []
      #   for db_prefix in args.databases:
      #      if db_prefix in dbs:
      #         databases.append(db_prefix)
      #      else:
      #         debug.graceful_exit("Input Error: Provided database was not "
      #                           "recognised! (%s)\n"%db_prefix)
   
  
   # Execute Program
   progname = 'SF'
   prog = Program(path='serotypefinder.pl',
      name=progname,
      args=['-d', args.db_dir,
            '-k', args.threshold,
            '-l', args.minlength,
            '-i', args.contigs,
            '-s', name
            ]
      )
   
   prog.execute()
   proglist.add2list(prog)
   prog.wait(interval=25)
   
   # THE SUCCESS OF THE PROGRAMS ARE VALIDATED
   results = {
      'genes': []
   }
   status = prog.get_status()
   
   # LOG THE TIMERS
   proglist.print_timers() 

if __name__ == '__main__':
   main()
